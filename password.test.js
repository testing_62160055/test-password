const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')

describe('Test Password Length', () => {
  test('should 8 character to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 character to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 character to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 character to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password to be true', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has not alphabet in password to be false', () => {
    expect(checkAlphabet('111')).toBe(false)
  })
  test('should has not alphabet A in password to be true', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
})

describe('Test Digit', () => {
  test('should has digit 1 in password to be true', () => {
    expect(checkDigit('1')).toBe(true)
  })
  test('should has not digit in password to be false', () => {
    expect(checkDigit('m')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol in password to be true', () => {
    expect(checkSymbol('!')).toBe(true)
  })
  test('should has not symbol in password to be false', () => {
    expect(checkSymbol('1A')).toBe(false)
  })
})

describe('Test password', () => {
  test('should password try#4567 to be true', () => {
    expect(checkPassword('try#4567')).toBe(true)
  })
  test('should password try#456 to be false', () => {
    expect(checkPassword('try#456')).toBe(false)
  })
  test('should password try#4561111111111111111111 to be false', () => {
    expect(checkPassword('try#4561111111111111111111')).toBe(false)
  })
  test('should password #4567890 to be false', () => {
    expect(checkPassword('#4567890')).toBe(false)
  })
  test('should password tryuiop# to be false', () => {
    expect(checkPassword('tryuiop#')).toBe(false)
  })
  test('should password tryu4567 to be false', () => {
    expect(checkPassword('tryu4567')).toBe(false)
  })
})
